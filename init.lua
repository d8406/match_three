-- Match Three init.lua
-- Copyright Duane Robertson (duane@duanerobertson.com), 2019
-- Distributed under the LGPLv2.1 (https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html)


match_three = {}
local mod = match_three
local mod_name = 'match_three'
local mod_path = minetest.get_modpath(mod_name)
local blocks = {}
local punched = {}
local add_time = 1
local match_check_time = 1

local axes = {'x', 'y', 'z'}
local node_below = vector.new(0, -1, 0)
local thick_air = mod_name..':thick_air'


if not minetest.get_modpath('mapgen')
and minetest.global_exists('dungeon_loot')
and dungeon_loot.populate_chest then
	-- This must be done after all items are defined.
	minetest.after(0, function()
		local options = {}
		options['default:acacia_wood']        =  {  0.5,   10,   nil   }
		--options['default:apple']            =  {  0.5,   4,    nil   }
		options['default:book']               =  {  0.5,   nil,  nil   }
		--options['default:coal_lump']        =  {  0.9,   12,   nil   }
		options['default:diamond']            =  {  0.2,   nil,  nil   }
		options['default:glass']              =  {  0.5,   5,    nil   }
		--options['default:gold_ingot']       =  {  0.5,   nil,  nil   }
		options['default:junglewood']         =  {  0.5,   10,   nil   }
		--options['default:mese_crystal']     =  {  0.1,   3,    nil   }
		options['default:meselamp']           =  {  0.1,   2,    nil   }
		--options['default:obsidian']         =  {  0.25,  3,    nil   }
		options['default:obsidian_glass']     =  {  0.1,   5,    nil   }
		options['default:obsidian_shard']     =  {  0.4,   3,    nil   }
		options['default:paper']              =  {  0.5,   5,    nil   }
		options['default:pick_diamond']       =  {  0.05,  nil,  nil   }
		options['default:pick_mese']          =  {  0.05,  nil,  nil   }
		--options['default:steel_ingot']      =  {  0.4,   6,    nil   }
		options['default:sword_diamond']      =  {  0.05,  nil,  nil   }
		options['default:sword_mese']         =  {  0.05,  nil,  nil   }
		options['default:wood']               =  {  0.5,   10,   nil   }
		options['fire:permanent_flame']       =  {  0.05,  nil,  nil   }
		options['dpies:onion']                =  {  0.4,   10,   nil   }
		options['dpies:apple_pie']            =  {  0.3,   10,   nil   }
		options['dpies:blueberry_pie']        =  {  0.3,   10,   nil   }
		options['dpies:meat_pie']             =  {  0.3,   10,   nil   }
		--options['booty:treasure_glass']     =  {  0.05,  nil,  nil   }
		options['dinv:leather_armor']         =  {  0.5,   nil,  nil   }
		options['dinv:leather_cap']           =  {  0.6,   nil,  nil   }
		options['dinv:boots']                 =  {  0.6,   nil,  nil   }
		options['dinv:wood_shield']           =  {  0.7,   nil,  nil   }
		options['dinv:steel_shield']          =  {  0.5,   nil,  nil   }
		options['dinv:chain_armor']           =  {  0.4,   nil,  nil   }
		options['dinv:plate_armor']           =  {  0.3,   nil,  nil   }
		options['dinv:ring_breath']           =  {  0.5,   nil,  nil   }
		options['dinv:ring_leap']             =  {  0.5,   nil,  nil   }
		options['dinv:bag_small']             =  {  0.8,   nil,  nil   }
		options['dinv:bag_medium']            =  {  0.6,   nil,  nil   }
		options['dinv:bag_large']             =  {  0.4,   nil,  nil   }
		options['dinv:fur_cloak']             =  {  0.7,   nil,  nil   }
		options['dinv:ring_protection_9']     =  {  0.5,   nil,  nil   }
		options['fun_tools:molotov_cocktail'] =  {  0.5,   3,    nil   }
		options['fun_tools:naptha']           =  {  0.5,   3,    nil   }
		options['fun_tools:flare_gun']        =  {  0.5,   nil,  nil   }
		options['tnt:gunpowder']              =  {  0.8,   10,   nil   }
		options['map:mapping_kit']            =  {  0.5,   3,    nil   }
		options['farming:cotton']             =  {  0.5,   5,    nil   }
		options['farming:flour']              =  {  0.5,   5,    nil   }
		options['farming:wheat']              =  {  0.5,   10,   nil   }
		-- The special items will NOT work in dungeon chests, natch.
		--options['bastet:bastet_paper']       =  {  0.1,   nil,  nil   }

		for name, desc in pairs(minetest.registered_items) do
			if name:find('^wool:') then
				options[name] = { 0.2, 2, nil }
			end
		end

		for name, d in pairs(options) do
			if minetest.registered_items[name] then
				local count = d[2] and {math.floor(d[2] / 2), d[2]} or nil
				local y = d[3] and {-31000, d[3]} or nil

				dungeon_loot.register({
					chance = d[1],
					count = count,
					name = name,
					y = y,
				})
			else
				print(mod_name..': * Could not register '..name)
			end
		end
	end)
end


local function find_matches(pos, nod, exclude)
	local ns = {x={}, y={}, z={}}
	if not pos then
		return 0, ns
	end

	if not nod then
		nod = minetest.get_node_or_nil(pos)
	end
	if not (nod and nod.name) then
		return 0, ns
	end

	local look_for = nod.name
	local mc = vector.new(0, 0, 0)
	for _, ax in pairs(axes) do
		local p3 = vector.new(pos)
		for k = -1, 1, 2 do
			for j = 1, 10 do
				p3[ax] = pos[ax] + j * k
				local n = minetest.get_node_or_nil(p3)
				if n and n.name == look_for
				and (not exclude or not vector.equals(p3, exclude)) then
					local nb = minetest.get_node_or_nil(vector.add(p3, node_below))
					if nb and (nb.name == 'air' or nb.name == thick_air) then
						break
					end
					mc[ax] = mc[ax] + 1
					table.insert(ns[ax], {pos=vector.new(p3), node=n})
				else
					break
				end
			end
		end
	end

	local maxm = math.max(mc.x, mc.y, mc.z)
	return maxm, ns
end


function mod.clone_node(name)
	if not (name and type(name) == 'string') then
		return
	end
	if not minetest.registered_nodes[name] then
		return
	end

	local nod = minetest.registered_nodes[name]
	local node2 = table.copy(nod)
	return node2
end


local all_blocks
local tnt_piece = mod_name..':tnt_piece'
function mod.add_blocks(pos)
	local t = minetest.get_node_timer(pos)
	local meta = minetest.get_meta(pos)
	if not (t and meta) then
		return
	end

	local score = meta:get_int('score')
	local v = vector.floor(vector.divide(pos, 2000))
	local max_score = math.max(math.abs(v.x), math.abs(v.y), math.abs(v.z)) + 20

	local p2 = vector.new(pos)
	p2.y = p2.y - 1
	local n = minetest.get_node_or_nil(p2)
	if n and (n.name == 'air' or n.name == thick_air) then
		local b = blocks[math.random(#blocks)]
		if score > 8 and b == blocks[#blocks] then
			b = tnt_piece
		end
		minetest.add_node(p2, {name=b})
		score = score + 1
		--if score > 20 then
		--	print(score)
		--end
		meta:set_int('score', score)
		local t2 = minetest.get_node_timer(p2)
		t2:start(math.random(50, 150) * match_check_time / 100)
		--minetest.check_for_falling(p2)
	end

	if not all_blocks then
		all_blocks = table.copy(blocks)
		all_blocks[#all_blocks+1] = mod_name..':top'
		all_blocks[#all_blocks+1] = mod_name..':top_crafted'
		all_blocks[#all_blocks+1] = mod_name..':tnt_piece'
		all_blocks[#all_blocks+1] = mod_name..':thick_air'
		all_blocks[#all_blocks+1] = mod_name..':clear_scrith'
	end

	if score > max_score then
		mod.clear_game(pos, score)
	elseif score < 0 then
		mod.clear_game(pos, score, true)
	end

	t:start(add_time)
end


function mod.clear_game(pos, score, loser)
	local p1 = vector.add(pos, -1)
	local p2 = vector.add(pos, 1)
	local ns = minetest.find_nodes_in_area(p1, p2, {mod_name..':top', mod_name..':top_crafted'})
	for _, p in pairs(ns) do
		local meta = minetest.get_meta(p)
		meta:set_int('score', score)
	end

	local p1 = table.copy(pos)
	local p2 = table.copy(pos)
	p1.y = p1.y - 20
	local pieces
	ns = minetest.find_nodes_in_area(p1, p2, all_blocks)
	for _, p in pairs(ns) do
		minetest.remove_node(p)
	end

	local p1 = vector.add(pos, -10)
	local p2 = vector.add(pos, 10)
	p2.y = pos.y
	local pieces
	ns = minetest.find_nodes_in_area(p1, p2, {mod_name..':clear_scrith'})
	for _, p in pairs(ns) do
		minetest.remove_node(p)
	end

	if not loser then
		local p1 = vector.add(pos, -10)
		local p2 = vector.add(pos, 10)
		p2.y = pos.y
		local pieces
		ns = minetest.find_nodes_in_area(p1, p2, {'default:chest'})
		if #ns > 0 then
			local meta = minetest.get_meta(ns[1])
			if not meta then
				return
			end
			local filled = meta:get_string('filled')
			if filled == 'yes' then
				return
			end
			meta:set_string('filled', 'yes')
			if minetest.global_exists('mapgen') and mapgen.fill_chest then
				mapgen.fill_chest(ns[1])
			elseif minetest.global_exists('dungeon_loot') and dungeon_loot.populate_chest then
				local ps = PcgRandom(os.time())
				dungeon_loot.populate_chest(ns[1], ps)
			else
				print(mod_name .. ': cannot fill chests without mapgen or patched dungeon_loot')
			end
		end
	end
end


function mod.clear_matches(pos)
	--pos = vector.round(pos)
	local t = minetest.get_node_timer(pos)
	local pb = vector.add(pos, node_below)
	local n = minetest.get_node_or_nil(pos)
	local nb = minetest.get_node_or_nil(pb)
	if not (t and n and nb) then
		return
	end

	if (nb.name == 'air' or nb.name == thick_air) then
		minetest.set_node(pos, {name=thick_air})
		minetest.add_node(pb, n)
		t = minetest.get_node_timer(pb)
		t:start(match_check_time)
		return
	end

	local ct, nodes = find_matches(pos)

	if ct < 2 then
		--minetest.check_for_falling(pos)
		-- Oddly enough, check_single is much, much worse.
		--minetest.check_single_for_falling(pos)
		t:start(match_check_time)
		return
	end

	local fal = {}
	for _, ax in pairs(axes) do
		if #nodes[ax] >= 2 then
			for _, n in pairs(nodes[ax]) do
				minetest.set_node(n.pos, {name=thick_air})
				if n.node.name == tnt_piece then
					tnt.boom(n.pos, {radius=5, disable_drops=true})

					for y = pos.y, pos.y + 10 do
						local p = {x=pos.x, y=y, z=pos.z}
						local n = minetest.get_node_or_nil(p)
						if n and (n.name == mod_name..':top' or n.name == mod_name..':top_crafted') then
							local meta = minetest.get_meta(p)
							meta:set_int('score', -999)
						end
					end
				end
				table.insert(fal, n.pos)
			end
		end
	end

	minetest.set_node(pos, {name=thick_air})
	--table.insert(fal, pos)
	--for _, p in pairs(fal) do
	--	minetest.check_for_falling(p)
	--end
end


function mod.cheat_check(pos)
	local t_air = 0
	for y = pos.y - 1, pos.y - 11, -1 do
		local n = minetest.get_node_or_nil(vector.new(pos.x, y, pos.z))
		if n and n.name == 'air' then
			t_air = t_air + 1
		else
			break
		end
	end
	if t_air == 0 or t_air > 10 then
		minetest.remove_node(pos)
		tnt.boom(pos, {radius=5, disable_drops=true})
	else
		minetest.set_node(vector.new(pos.x, pos.y - t_air, pos.z), { name = mod_name..':clear_scrith' })
	end
end


function mod.punch_it(pos, node, puncher, pointed_thing)
	local player_name = puncher:get_player_name()
	if not player_name then
		return
	end

	if not punched[player_name] then
		punched[player_name] = pos
	else
		local p2 = punched[player_name]
		punched[player_name] = nil
		if p2 == pos then
			-- nop
		elseif vector.distance(pos, p2) > 1 then
			return
		else
			local n1 = node
			local n2 = minetest.get_node_or_nil(p2)
			if not (n1 and n2) then
				return
			end

			local n1_ct, n1_m = find_matches(pos, n2, p2)
			local n2_ct, n2_m = find_matches(p2, n1, pos)
			--print(n1.name, n1_ct)
			--print(n2.name, n2_ct)
			--print(n1.name, n1_ct, dump(n1_m))
			--print(n2.name, n2_ct, dump(n2_m))

			if n1_ct < 2 and n2_ct < 2 then
				return
			end

			minetest.set_node(p2, n1)
			local t = minetest.get_node_timer(p2)
			t:start(match_check_time)
			minetest.set_node(pos, n2)
			t = minetest.get_node_timer(pos)
			t:start(match_check_time)
		end
	end

	--[[
	-------------------------------------
	local t1 = os.clock()
	do
		local p1 = vector.subtract(pos, 10)
		local p2 = vector.add(pos, 10)
		local ns
		for i = 1, 1000 do
			--ns = minetest.find_nodes_in_area(p1, p2, {'group:match_three'})
			ns = minetest.find_nodes_in_area(p1, p2, {mod_name..':steel_piece'})
		end

		print('--------------------------')
		print('time 1: ' .. math.floor((os.clock() - t1) * 1000), #ns)
		--print(dump(ns))
		print('--------------------------')
	end

	t1 = os.clock()
	do
		local axes = {'x', 'y', 'z'}
		local p1 = vector.new(pos)
		local p2 = vector.new(pos)
		p2.y = p2.y - 1
		local ns, mc
		local ct = 0
		for i = 1, 1000 do
			for _, p in pairs({p2, p1}) do
				mc, ns = find_matches(p, {name=mod_name..':steel_piece'})
			end
		end
		print('--------------------------')
		print('time 2: ' .. math.floor((os.clock() - t1) * 1000), #ns)
		--print(dump(ns))
		print('--------------------------')
	end
	-------------------------------------
	--]]
end


local function register_token_node(nam, tile)
	local full_name = mod_name..':'..nam..'_piece'
	local def = {
		description = 'Match Three Token',
		paramtype = 'light',
		tiles = {tile},
		--groups = {falling_node=1, match_three=1},
		groups = {unbreakable=1, match_three=1},
		sounds = default.node_sound_stone_defaults(),
		sunlight_propagates = true,
		light_source = 8,
		diggable = false,
		on_blast = function(...)
		end,
		on_construct = function(pos)
			local t = minetest.get_node_timer(pos)
			t:start(match_check_time)
		end,
        on_timer = mod.clear_matches,
        on_punch = mod.punch_it,
	}
	if nam:find('glass') then
		def.drawtype = 'glasslike_framed'
		--def.tiles = {'default_glass.png', 'default_glass_detail.png'}
	elseif nam:find('tnt') then
		def.tiles = {'tnt_top.png', 'tnt_bottom.png', 'tnt_side.png'}
	end
	minetest.register_node(full_name, def)
	if not nam:find('tnt') then
		table.insert(blocks, full_name)
	end
end

for _, n in pairs({
	{'stone', 'default_stone_block.png'},
	{'desert_stone', 'default_desert_stone_block.png'},
	{'sandstone', 'default_sandstone_block.png'},
	{'glass', 'default_glass.png'},
	{'tnt', 'tnt.png'},
	{'copper', 'default_copper_block.png'},
	{'steel', 'default_steel_block.png'},
	--{'wood', 'default_wood.png'},
}) do
	register_token_node(n[1], n[2])
end


do
	local n = mod.clone_node('default:obsidian')
	n.description = 'Match Three Top'
	n.diggable = false
	n.drop = ''
	n.groups = {unbreakable=1}
	n.light_source = 8
	n.on_blast = function(...)
	end
	n.on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_int('score', 0)
		local t = minetest.get_node_timer(pos)
		t:start(add_time)
	end
	n.on_timer = mod.add_blocks
	minetest.register_node(mod_name..':top', n)

	n = mod.clone_node('air')
	n.walkable = true
	n.groups = {unbreakable=1}
	n.on_blast = function(...)
	end
	minetest.register_node(thick_air, n)


	minetest.register_node(mod_name..':clear_scrith', {
		description = 'Black Scrith',
		drawtype = 'glasslike_framed_optional',
		--paramtype = 'light',
		sunlight_propagates = true,
		tiles = {'default_obsidian_glass.png', 'default_obsidian_glass_detail.png'},
		--light_source = 1,
		--use_texture_alpha = true,
		is_ground_content = false,
		groups = {unbreakable=1},
		sounds = default.node_sound_glass_defaults(),
		on_blast = function(...)
		end,
	})
end

do
	local n = mod.clone_node('default:obsidian')
	n.description = 'Match Three Top'
	n.drop = ''
	n.light_source = 8
	--n.on_blast = function(...)
	--end
	n.on_construct = function(pos)
		mod.cheat_check(pos)
		local meta = minetest.get_meta(pos)
		meta:set_int('score', 0)
		local t = minetest.get_node_timer(pos)
		t:start(add_time)
	end
	n.on_dig = function(pos, node, digger)
		minetest.node_dig(pos, node, digger)
		mod.clear_game(pos, 0, true)
	end
	n.on_timer = mod.add_blocks
	minetest.register_node(mod_name..':top_crafted', n)

	minetest.register_craft({
		output = mod_name..':top_crafted 24',
		recipe = {
			{ 'default:sand', '', 'default:gravel' },
			{ '', 'default:glass', '' },
			{ 'default:cobble', '', 'default:steel_ingot' },
		},
	})
end
